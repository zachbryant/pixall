const path = require("path");

let isDev = process.env.NODE_ENV !== "production";

module.exports = {
  lintOnSave: isDev,
  devServer: {
    open: true,
    host: "0.0.0.0",
    port: 8080,
    https: false,
    hotOnly: false,
    overlay: {
      warnings: false,
      errors: true
    }
  },
  configureWebpack: {
    resolve: {
      extensions: [".js", ".vue", ".json"],
      alias: {
        "@": path.resolve(__dirname, "src")
      }
    }
  },
  publicPath: !isDev ? "client/dist/" : "client/",
  outputDir: "client/dist/"
};