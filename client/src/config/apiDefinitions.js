let isDev = process.env.NODE_ENV !== "production";

let BASE;
if (isDev) BASE = "http://localhost:5000";
else BASE = "http://www.pixall.top";

let API = {
  BASE: BASE,
  AUTH: BASE + "/api/auth/",
  PROJECT: BASE + "/api/project/"
};
API.USER_IDENTITY = API.AUTH + "user/";
API.LOGIN = API.AUTH + "login/";
API.LOGOUT = API.AUTH + "logout/";

module.exports = {
  API
};
