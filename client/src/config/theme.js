import Vue from "vue";
import LRU from "lru-cache";
import Vuetify from "vuetify";
import VueNoty from "vuejs-noty";
import "vuetify/dist/vuetify.min.css";
import VeeValidate from "vee-validate";
import "vuejs-noty/dist/vuejs-noty.css";
import colors from "vuetify/es5/util/colors";
import "@fortawesome/fontawesome-free/css/all.css";
//import { VueSpinners } from "@saeris/vue-spinners";
import "roboto-fontface/css/roboto/roboto-fontface.css";

const themeCache = new LRU({
  max: 10,
  maxAge: 1000 * 60 * 60 // 1 hour
});

Vue.use(Vuetify, {
  iconfont: "fa",
  theme: {
    primary: "#AA57EB",
    secondary: "#C07EF2",
    accent: "#ebc651",
    error: "#C44A66",
    info: "#AA57EB",
    success: "#AA57EB",
    warning: "#ebc651"
  },
  options: {
    customProperties: true,
    minifyTheme: function(css) {
      return process.env.NODE_ENV === "production"
        ? css.replace(/[\s|\r\n|\r|\n]/g, "")
        : css;
    },
    themeCache
  }
});
Vue.use(VueNoty, {
  type: "info",
  timeout: 3000,
  progressBar: false,
  layout: "bottomCenter",
  animation: {
    open: "animated slideInUp", // Animate.css class names
    close: "animated slideOutDown faster" // Animate.css class names
  },
  killer: true
});
//Vue.use(VueSpinners);
Vue.use(VeeValidate);
