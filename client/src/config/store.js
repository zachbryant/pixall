import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";

Vue.use(Vuex);

const uuidv4 = require("uuid/v4");
import { API } from "./apiDefinitions";
let isDev = process.env.NODE_ENV !== "production";
export default new Vuex.Store({
  state: {
    user: localStorage.getItem("user") || null,
    token: localStorage.getItem("token") || "",
    isLoading: false,
    isDev: isDev,
    pixelSize: 50,
    currentTool: "brush",
    currentProject: {
      pid: uuidv4(),
      data: {
        currentColor: "#AA57EB",
        forgroundColor: "#AA57EB",
        backgroundColor: "#FFFFFF",
        resolution: {
          x: 64,
          y: 64
        },
        image: []
      }
    },
    noty: { closed: true },
    madeEdits: false
  },
  getters: {
    isDev: state => state.isDev,
    isLoading: state => state.isLoading,
    isLoggedIn: state => !!state.token,
    authStatus: state => state.status,
    getUser: state => state.user,
    currentColor: state => state.currentProject.data.currentColor,
    forgroundColor: state => state.currentProject.data.forgroundColor,
    backgroundColor: state => state.currentProject.data.backgroundColor,
    numCols: state => state.currentProject.data.resolution.x,
    numRows: state => state.currentProject.data.resolution.y,
    pixelSize: state => state.pixelSize,
    currentProject: state => state.currentProject,
    currentPid: state => state.currentProject.pid,
    currentTool: state => state.currentTool,
    imageArray: state => state.currentProject.data.image,
    canNoty: state => state.noty.closed,
    madeEdits: state => state.madeEdits
  },
  mutations: {
    newProject(state) {
      state.madeEdits = false;
      let newPid = uuidv4();
      state.currentProject = {
        pid: newPid,
        data: {
          currentColor: "#AA57EB",
          forgroundColor: "#AA57EB",
          backgroundColor: "#FFFFFF",
          resolution: {
            x: 64,
            y: 64
          },
          image: []
        }
      };
    },
    setNoty(state, noty) {
      state.noty = noty;
    },
    setCurrentProject(state, project) {
      state.currentProject = project;
      state.madeEdits = true;
    },
    setCurrentTool(state, tool) {
      state.currentTool = tool;
    },
    setNumCols(state, numCols) {
      state.currentProject.data.resolution.x = numCols;
      state.madeEdits = true;
    },
    setNumRows(state, numRows) {
      state.currentProject.data.resolution.y = numRows;
      state.madeEdits = true;
    },
    setImageArray(state, imageArray) {
      state.currentProject.data.image = imageArray;
      state.madeEdits = true;
    },
    setPixelSize(state, pixelSize) {
      state.pixelSize = pixelSize;
    },
    setCurrentColor(state, color) {
      state.currentProject.data.currentColor = color;
    },
    setLoading(state, isLoading) {
      state.isLoading = isLoading;
    },
    login(state, { user, token }) {
      localStorage.setItem("token", token);
      Axios.defaults.headers.common["Authorization"] = `JWT ${token}`;
      state.token = token;
      state.user = user;
    },
    logout(state) {
      state.token = "";
      state.user = "";
      localStorage.removeItem("token");
      delete Axios.defaults.headers.common["Authorization"];
    }
  },
  actions: {
    download({ state }, { project, multiplyBy }) {
      if (!project) project = state.currentProject;
      let numCols = project.data.resolution.x;
      let numRows = project.data.resolution.y;
      let image = project.data.image;
      return new Promise((resolve, reject) => {
        var hcanv = document.createElement("canvas");
        hcanv.style.display = "none";
        document.body.appendChild(hcanv);
        hcanv.width = numCols * multiplyBy;
        hcanv.height = numRows * multiplyBy;
        var hctx = hcanv.getContext("2d");

        // Draw pixels
        for (var i = 0; i < numCols; i++) {
          for (var j = 0; j < numRows; j++) {
            if (image[i] && image[i][j]) {
              hctx.fillStyle = image[i][j];
              hctx.fillRect(
                i * multiplyBy,
                j * multiplyBy,
                multiplyBy,
                multiplyBy
              );
            }
          }
        }

        var url = document.createElement("a");
        document.body.appendChild(url);
        url.download = "image.png";
        url.href = hcanv.toDataURL("image/png");
        resolve(url);

        hcanv.parentNode.removeChild(hcanv);
      });
    },
    newProject({ commit, dispatch, state }) {
      return new Promise((resolve, reject) => {
        commit("newProject");
        resolve();
      });
    },
    getProject({ commit, state }, params) {
      return new Promise((resolve, reject) => {
        var queryString = Object.keys(params)
          .map(key => {
            if (params[key]) return key + "=" + params[key];
            return "";
          })
          .filter(Boolean)
          .join("&");
        Axios({
          url: API.PROJECT + "?" + queryString,
          method: "GET"
        })
          .then(resp => {
            let data = resp.data.project;
            if (data) {
              commit("setCurrentProject", data);
            }
            resolve(resp);
          })
          .catch(err => {
            //commit("setCurrentProject", {});
            reject(err);
          });
      });
    },
    updateProject({ state }) {
      return new Promise((resolve, reject) => {
        Axios({
          url: API.PROJECT,
          data: {
            project: state.currentProject
          },
          method: "PUT"
        })
          .then(resp => {
            resolve(resp);
          })
          .catch(err => {
            reject(err.response);
          });
      });
    },
    deleteProject({ state }, project) {
      return new Promise((resolve, reject) => {
        Axios({
          url: API.PROJECT,
          data: {
            project
          },
          method: "DELETE"
        })
          .then(resp => {
            resolve(resp);
          })
          .catch(err => {
            reject(err.response);
          });
      });
    },
    login({ commit }, credentials) {
      return new Promise((resolve, reject) => {
        Axios({
          url: API.LOGIN,
          data: credentials,
          method: "POST"
        })
          .then(resp => {
            commit("login", {
              user: resp.data.user,
              token: resp.data.token
            });
            resolve(resp);
          })
          .catch(err => {
            commit("logout");
            reject(err);
          });
      });
    },
    logout({ commit }) {
      // eslint-disable-next-line no-unused-vars
      return new Promise((resolve, reject) => {
        Axios({
          url: API.LOGOUT,
          method: "GET"
        })
          .then(resp => {
            commit("logout");
            resolve(resp);
          })
          .catch(err => {
            reject(err.response);
          });
      });
    }
  },
  strict: isDev
});
