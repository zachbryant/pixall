import Vue from "vue";
import Router from "vue-router";
import Home from "../views/Home";
import store from "./store";

Vue.use(Router);

function isObjectEmpty(obj) {
  return Object.keys(obj).length === 0 && obj.constructor === Object;
}

let router = new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "home",
      component: Home,
      meta: {
        requiresAuth: true,
        requiresAuthQueries: true
      },
      props: route => ({
        query: {
          pid: route.query.pid
        }
      })
    },
    {
      path: "/login",
      name: "login",
      component: () => import("../views/Login.vue")
    },
    {
      path: "/logout",
      name: "logout",
      component: () => import("../views/Login.vue"),
      props: {
        logoutProp: true
      }
    },
    {
      path: "/gallery",
      name: "gallery",
      component: () => import("../views/Gallery.vue"),
      meta: {
        requiresAuth: true,
        requiresAuthQueries: true
      }
    },
    {
      path: "*",
      redirect: "/"
    }
  ]
});

router.beforeEach((to, from, next) => {
  if (
    to.matched.some(record => {
      return (
        !!record.meta.requiresAuth ||
        (!isObjectEmpty(to.query) && !!record.meta.requiresAuthQueries)
      );
    })
  ) {
    if (store.getters.isLoggedIn) {
      next();
      return;
    }
    next({
      name: "login",
      path: "/login",
      props: to.props,
      meta: to.meta,
      query: to.query,
      params: {
        redirect: {
          path: to.fullPath,
          props: to.props,
          query: to.query
        }
      }
    });
  } else {
    next();
  }
});

export default router;
