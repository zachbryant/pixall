import Vue from "vue";
import Axios from "axios";
import "es6-promise/auto";
import App from "./App.vue";

// Config
import "./config/theme";
import store from "./config/store";
import router from "./config/router";
Vue.config.productionTip = false;

Vue.prototype.$http = Axios;
// Set Axios authorization to token
const token = localStorage.getItem("token");
if (token) {
  Vue.prototype.$http.defaults.headers.common["Authorization"] = `JWT ${token}`;
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount("#app");
