var Camera = function(x, y, scale, rotation) {
  //everything needed for default camera
  this.x = x || 0;
  this.y = y || 0;
  this.scale = scale || 1;
  this.rotation = rotation || 0;
};
Camera.prototype.apply = function(canvas, ctx) {
  var centerX = canvas.width / (2 * this.scale);
  var centerY = canvas.height / (2 * this.scale);
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  ctx.save();
  ctx.scale(this.scale, this.scale);
  ctx.translate(centerX, centerY);
  ctx.rotate(this.rotation);
  ctx.translate(-this.x, -this.y);
};
Camera.prototype.remove = function(ctx) {
  ctx.restore();
};
Camera.prototype.moveTo = function(x, y) {
  this.x = x;
  this.y = y;
};
Camera.prototype.move = function(dx, dy) {
  this.x = this.x + (dx * 1) / this.scale;
  this.y = this.y + (dy * 1) / this.scale;
};
Camera.prototype.toMapCoords = function(canvas, x, y) {
  x = (x - canvas.width / 2) / this.scale;
  y = (y - canvas.height / 2) / this.scale;
  var cos = Math.cos(-this.rotation);
  var sin = Math.sin(-this.rotation);
  x = cos * x - sin * y;
  y = sin * x + cos * y;
  return {
    x: x + this.x,
    y: y + this.y
  };
};
Camera.prototype.toCameraCoords = function(canvas, x, y) {
  x = x - this.x;
  y = y - this.y;
  var cos = Math.cos(this.rotation);
  var sin = Math.sin(this.rotation);
  x = cos * x - sin * y;
  y = sin * x + cos * y;
  return {
    x: x * this.scale + canvas.width / 2,
    y: y * this.scale + canvas.height / 2
  };
};
Camera.prototype.scaleBy = function(scalar) {
  this.scale *= scalar;
};
Camera.prototype.scaleToPoint = function(canvas, scalar, pointx, pointy) {
  var relCenterX = canvas.width / 2 - pointx;
  var relCenterY = canvas.height / 2 - pointy;
  var moveX = relCenterX / (this.scale * scalar) - relCenterX / this.scale;
  var moveY = relCenterY / (this.scale * scalar) - relCenterY / this.scale;
  this.move(moveX, moveY);
  this.scaleBy(scalar);
};

module.exports = Camera;
