# pixall
Online pixel art creator  

CJ Enright - cenrigh@purdue.edu  
Ankit Agrawal - agrawa62@purdue.edu  
Zach Bryant - bryant98@purdue.edu  

Pixall – Pixel art editor with cloud saving  

Backend  – Express.js and Node.js  
Database – MongoDB  
Frontend – Vue and Canvas element  
