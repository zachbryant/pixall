import { Schema, model } from "mongoose";
import uuidv4 from "uuid/v4";
const code_gen = require("../util/code_gen");
const mailer = require("../util/mail");

let timerMap = {};

var userSchema = new Schema({
  uid: {
    type: String,
    default: uuidv4()
  },
  email: {
    type: String,
    index: true,
    unique: true,
    required: true
  },
  code: {
    type: String,
    default: ""
  },
  attempts: {
    type: Number,
    default: 0
  },
  lastAttempt: {
    type: Date,
    default: null
  },
  visits: {
    type: Number,
    default: 0
  }
});

// For doing general work on "Users"
userSchema.statics = {
  byUid: function(uid, callback) {
    return this.findOne(
      {
        uid: uid
      },
      callback
    );
  },
  byUidArray: function(uids, callback) {
    return this.find(
      {
        uid: {
          $in: uids
        }
      },
      callback
    );
  },
  byEmail: function(email, callback) {
    return this.findOne(
      {
        email: new RegExp(email, "i")
      },
      callback
    );
  }
};

// For doing instance work on a User
userSchema.methods = {
  clearCode: function() {
    this.code = "";
    this.markModified("code");
  },
  setCode: function(code) {
    this.code = code;
    this.markModified("code");
    this.incrementAttempts();
    var prevTimer = timerMap[this.uid];
    if (prevTimer) {
      clearTimeout(prevTimer);
    }
    let self = this;
    timerMap[this.uid] = setTimeout(function() {
      self.clearCode();
    }, 5 * 60 * 1000);
  },
  newCode(callback) {
    if (!this.canGetCode()) {
      callback({
        message: "Cool your jets! Wait 3 minutes."
      });
    } else {
      const code = code_gen.randPassword();
      this.setCode(code, callback);
      mailer.sendLoginCode(this.email, code);
    }
  },
  incrementVisits() {
    this.visits++;
    this.markModified("visits");
  },
  incrementAttempts() {
    this.attempts++;
    if (this.canGetCode()) this.lastAttempt = new Date();
    this.markModified("attempts");
    this.markModified("lastAttempt");
  },
  resetAttempts() {
    this.attempts = 0;
    this.markModified("attempts");
  },
  isOverLimit() {
    return this.attempts >= 5;
  },
  minsSinceLastAttempt() {
    let lastDate = this.lastAttempt || new Date();
    let diffMs = new Date() - lastDate;
    return Math.round(((diffMs % 86400000) % 3600000) / 60000);
  },
  canGetCode() {
    return !(this.isOverLimit() && this.minsSinceLastAttempt() < 3);
  },
  isCodeValid() {
    return this.minsSinceLastAttempt() < 5;
  }
};

var User = model("User", userSchema, "Users");

module.exports = User;
