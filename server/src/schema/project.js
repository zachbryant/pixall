import { Schema, model } from "mongoose";

var projectSchema = new Schema({
  uid: String,
  pid: String,
  data: Schema.Types.Mixed
});

projectSchema.index({
  uid: 1,
  pid: 1
});

projectSchema.statics = {
  byUser: function(user, callback) {
    return this.find(
      {
        uid: user instanceof Object ? user.uid : user
      },
      callback
    );
  },
  byProject: function(project, callback) {
    return this.find(
      {
        pid: project instanceof Object ? project.pid : project
      },
      callback
    );
  },
  hasAccess: function(user, project, callback) {
    return this.findOne(
      {
        uid: user instanceof Object ? user.uid : user,
        pid: project instanceof Object ? project.pid : project
      },
      callback
    );
  }
};

var Project = model("Project", projectSchema, "Projects");

module.exports = Project;
