import https from "https";
const path = require("path");
import { readFileSync } from "fs";
const express = require("express");

var privateKey = readFileSync(path.resolve(__dirname, "./cert/cert.key"));
var certificate = readFileSync(path.resolve(__dirname, "./cert/cert.pem"));
var credentials = {
  key: privateKey,
  cert: certificate
};

const app = express();
var server = https.createServer(credentials, app);
const port = process.env.PORT || 5000;
server.listen(port, () => console.log(`Server started on port ${port}`));
app.get("*", function(req, res) {
  res.redirect("https://pixall.top" + req.url);
});

module.exports = app;
