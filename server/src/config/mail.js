var fromHeader = '"Pixall" <no-reply@pixall.top>';

module.exports = {
	transport: {
		host: "mail.privateemail.com",
		port: 465,
		secure: true,
		auth: {
			user: "no-reply@pixall.top",
			pass: "pixalllab6",
		},
	},
	options: {
		from: fromHeader,
	},
	loginEmailOptions: function(email, code) {
		return {
			from: fromHeader,
			subject: "🎨 Pixall temporary login code",
			to: email,
			html: `<div
        style="margin: 0 auto;width: 100%; max-width: 600px;font-weight:500; color: #333333;font-family:sans-serif;"
      >
        <h4>Howdy,</h4>
        <p>Your temporary login code is</p>
        <pre
          style="font-family:monospace;background-color:#F4F4F4;border-radius:3px;margin-bottom:24px;padding:16px 24px;border:1px solid #EEEEEE;"
				><span id="pixcode">${code}</span><img id="clip" src="https://i.imgur.com/F9kGJru.png" style="float:right;"></img></pre>
				<p>If you didn't request this code, you can safely ignore this email.</p>
				&nbsp;
        <p>Best,</p>
				<p style="font-weight: 400;font-style: italic;">The Pixall Team</p>
				<a style="color: #9A6BB0;" href="mailto:admin@pixall.top">Contact Us</a>
      </div>
  `,
		};
	},
};
