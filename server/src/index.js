/* eslint-disable no-console */
const cors = require("cors");
const express = require("express");
const passport = require("passport");
const bodyParser = require("body-parser");
var serveStatic = require("serve-static");
import { clientDist } from "./util/util";

// Configuration
//const app = require("./config/secure");
const app = express();
require("./config/passport");
require("./config/mongo");
require("./config/jwtConfig");

// Middleware
app.use(
  bodyParser.json({
    limit: "50mb",
    extended: true
  })
);
app.use(
  bodyParser.urlencoded({
    limit: "50mb",
    extended: true
  })
);
app.use(cors());
app.use(passport.initialize()); // Start using passport
app.use(passport.session()); // Session management

// Routes
// Catch all routes and redirect to the index file
console.log(clientDist);
app.use("/", serveStatic(clientDist));
const staticRoute = require("./api/static");
app.use("/", staticRoute);
const auth = require("./api/auth");
app.use("/api/auth", auth);
const project = require("./api/project");
app.use("/api/project", project);

const port = process.env.PORT || 5000;
app.listen(port, () => console.log(`Server started on port ${port}`));
