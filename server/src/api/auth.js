/* eslint-disable no-console */
const express = require("express");
const passport = require("passport");
const jwt = require("jsonwebtoken");
const jwtConfig = require("../config/jwtConfig");
const models = require("../schema/models");
const User = models.User;
const util = require("../util/util");
var router = express.Router();

// This all happens on /api/auth

router.post("/login", (req, res, next) => {
  const code = req.body.code;
  const email = req.body.email;
  if (code) {
    passport.authenticate("login", (err, user, info) => {
      if (err) return next(err);
      if (!user || !user.isCodeValid()) {
        return res.status(401).send([user, "Cannot log in", info]);
      }
      user.resetAttempts();
      user.incrementVisits();
      user.save().catch(err => {
        console.error(err);
      });
      const payload = {
        uid: user.uid,
        expires: Date.now() + 24 * 60 * 60 * 1000
      };
      req.login(
        payload,
        {
          session: false
        },
        err => {
          if (err) {
            res.status(500).send({
              err
            });
          } else {
            const token = jwt.sign(JSON.stringify(payload), jwtConfig.secret);
            res.status(200).send({
              token: token,
              user: user,
              message: "Welcome" + (user.visits > 1 ? " back" : "") + " :)"
            });
          }
        }
      );
    })(req, res, next);
  } else if (email) {
    User.byEmail(email, function(err, user) {
      if (err) {
        console.error(err);
        res.status(500).send({
          message: "Had a problem looking up email"
        });
      } else if (user) {
        if (user.canGetCode()) {
          user.newCode();
          user.save().catch(err => console.error(err));
          res.status(200).send();
        } else {
          res.status(429).send();
        }
      } else {
        User.create(
          {
            email
          },
          function(err1, newUser) {
            if (newUser) {
              newUser.newCode();
              newUser.save();
              res.status(200).send();
            } else {
              if (err1) console.error(err1);
              res.status(500).send({
                message: "Had a problem creating user"
              });
            }
          }
        );
      }
    });
  } else {
    res.status(400).send({
      message: "An email is required. Are you using x-www-form-urlencoded?"
    });
  }
});

router.get("/logout", (req, res) => {
  req.logout();
  return res.status(200).send({
    message: "Logged out"
  });
});

router.get(
  "/user",
  passport.authenticate("JWT", {
    session: false
  }),
  (req, res) => {
    res.send({
      user: req.user
    });
  }
);

module.exports = router;
