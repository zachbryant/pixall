/* eslint-disable no-console */
const uuidv4 = require("uuid/v4");
const express = require("express");
const passport = require("passport");
import { Project } from "../schema/models";

var router = express.Router();

router.get(
  "/",
  passport.authenticate("JWT", {
    session: false
  }),
  (req, res) => {
    let user = req.user;
    let query = req.query;
    let pid = query.pid;
    if (pid) {
      Project.hasAccess(user, { pid }, function(err, access) {
        if (err) {
          console.log(err);
          res.status(500).send({
            message: "Database lookup error"
          });
        } else if (access) {
          console.log(access);
          res.status(200).send({
            project: access
          });
        } else {
          res.status(404).send({
            message: "You're not allowed to access that."
          });
        }
      });
    } else {
      Project.byUser(user, function(err, project) {
        if (err) {
          console.log(err);
          res.status(500).send({
            message: "Database lookup error"
          });
        } else if (project) {
          if (project instanceof Array) {
            res.status(200).send({ projects: project });
          } else res.status(200).send({ project });
        } else {
          res.status(404).send({
            message: "No projects found... start one?"
          });
        }
      });
    }
  }
);

router.delete(
  "/",
  passport.authenticate("JWT", {
    session: false
  }),
  (req, res) => {
    let user = req.user;
    let project = req.body.project;
    let uid = user.uid;
    let pid = project instanceof Object ? project.pid : project;
    if (uid && pid) {
      Project.findOneAndRemove({ uid, pid }, function(err, ok) {
        if (err) {
          console.log(err);
          res.status(500).send({
            message: "Database lookup error"
          });
        } else if (ok) {
          res.status(200).send();
        } else {
          res.status(404).send({
            message: "No records for user access in the database"
          });
        }
      });
    } else
      res.status(400).send({
        message:
          "Expected user & project with pid but got \n" +
          JSON.stringify({
            user,
            project
          })
      });
  }
);

router.put(
  "/",
  passport.authenticate("JWT", {
    session: false
  }),
  (req, res) => {
    let user = req.user;
    let project = req.body.project;
    let uid = user.uid;
    console.log("UID: " + uid);
    let pid = project.pid;
    console.log("PID: " + pid);
    let data = project.data;
    if (uid && pid && data) {
      Project.findOneAndUpdate(
        {
          uid,
          pid
        },
        {
          uid,
          pid,
          data
        },
        {
          upsert: true,
          new: true
        },
        function(err, ok) {
          if (err) {
            console.log(err);
            res.status(500).send({
              message: "Database lookup error"
            });
          } else if (ok) {
            res.status(200).send();
          } else {
            res.status(404).send({
              message: "No records for user access in the database"
            });
          }
        }
      );
    } else
      res.status(400).send({
        message:
          "Expected user & project with pid & data but got \n" +
          JSON.stringify({
            user,
            project
          })
      });
  }
);

module.exports = router;
