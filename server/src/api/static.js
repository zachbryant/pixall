/* eslint-disable no-console */
const express = require("express");
import { clientDist } from "../util/util";

var router = express.Router();

const staticServeFunc = function(req, res) {
  res.sendFile(clientDist + "/index.html");
};

let staticRoutes = ["/", "/login", "/logout", "/gallery"];
for (var index in staticRoutes) {
  var str = staticRoutes[index];
  console.log(str + " -> " + clientDist + "/index.html");

  router.get(str, staticServeFunc);
}

module.exports = router;
