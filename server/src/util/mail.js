const nodemailer = require("nodemailer");
const createTransport = nodemailer.createTransport;
const emailConfig = require("../config/mail");

const transporter = createTransport(emailConfig.transport);

function sendLoginCode(email, code) {
  transporter.sendMail(
    emailConfig.loginEmailOptions(email, code),
    (err, info) => {
      if (err || !info) {
        console.error("Could not send mail:");
        if (err) console.error(err);
      }
    }
  );
}

module.exports = {
  emailConfig,
  sendLoginCode
};
