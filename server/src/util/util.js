import uuidv4 from "uuid/v4";
var path = require("path");

function getUUIDV4() {
  return uuidv4();
}

function isValidJWT(jwt_payload) {
  return !jwt_payload.expires || jwt_payload.expires > Date.now();
}

let clientDist = path.resolve(__dirname, "..", "../../client/dist");

module.exports = {
  isValidJWT,
  getUUIDV4,
  clientDist
};
