"use strict";

var _interopRequireDefault = require("/home/zach/mega/repos/web/pixall/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

var _stringify = _interopRequireDefault(require("/home/zach/mega/repos/web/pixall/node_modules/@babel/runtime-corejs2/core-js/json/stringify"));

var _models = require("../schema/models");

/* eslint-disable no-console */
var uuidv4 = require("uuid/v4");

var express = require("express");

var passport = require("passport");

var router = express.Router();
router.get("/", passport.authenticate("JWT", {
  session: false
}), function (req, res) {
  var user = req.user;
  var query = req.query;
  var pid = query.pid;

  if (pid) {
    _models.Project.hasAccess(user, {
      pid: pid
    }, function (err, access) {
      if (err) {
        console.log(err);
        res.status(500).send({
          message: "Database lookup error"
        });
      } else if (access) {
        console.log(access);
        res.status(200).send({
          project: access
        });
      } else {
        res.status(404).send({
          message: "You're not allowed to access that."
        });
      }
    });
  } else {
    _models.Project.byUser(user, function (err, project) {
      if (err) {
        console.log(err);
        res.status(500).send({
          message: "Database lookup error"
        });
      } else if (project) {
        if (project instanceof Array) {
          res.status(200).send({
            projects: project
          });
        } else res.status(200).send({
          project: project
        });
      } else {
        res.status(404).send({
          message: "No projects found... start one?"
        });
      }
    });
  }
});
router.delete("/", passport.authenticate("JWT", {
  session: false
}), function (req, res) {
  var user = req.user;
  var project = req.body.project;
  var uid = user.uid;
  var pid = project instanceof Object ? project.pid : project;

  if (uid && pid) {
    _models.Project.findOneAndRemove({
      uid: uid,
      pid: pid
    }, function (err, ok) {
      if (err) {
        console.log(err);
        res.status(500).send({
          message: "Database lookup error"
        });
      } else if (ok) {
        res.status(200).send();
      } else {
        res.status(404).send({
          message: "No records for user access in the database"
        });
      }
    });
  } else res.status(400).send({
    message: "Expected user & project with pid but got \n" + (0, _stringify.default)({
      user: user,
      project: project
    })
  });
});
router.put("/", passport.authenticate("JWT", {
  session: false
}), function (req, res) {
  var user = req.user;
  var project = req.body.project;
  var uid = user.uid;
  console.log("UID: " + uid);
  var pid = project.pid;
  console.log("PID: " + pid);
  var data = project.data;

  if (uid && pid && data) {
    _models.Project.findOneAndUpdate({
      uid: uid,
      pid: pid
    }, {
      uid: uid,
      pid: pid,
      data: data
    }, {
      upsert: true,
      new: true
    }, function (err, ok) {
      if (err) {
        console.log(err);
        res.status(500).send({
          message: "Database lookup error"
        });
      } else if (ok) {
        res.status(200).send();
      } else {
        res.status(404).send({
          message: "No records for user access in the database"
        });
      }
    });
  } else res.status(400).send({
    message: "Expected user & project with pid & data but got \n" + (0, _stringify.default)({
      user: user,
      project: project
    })
  });
});
module.exports = router;