"use strict";

var _interopRequireDefault = require("/home/zach/mega/repos/web/pixall/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

var _stringify = _interopRequireDefault(require("/home/zach/mega/repos/web/pixall/node_modules/@babel/runtime-corejs2/core-js/json/stringify"));

var _now = _interopRequireDefault(require("/home/zach/mega/repos/web/pixall/node_modules/@babel/runtime-corejs2/core-js/date/now"));

/* eslint-disable no-console */
var express = require("express");

var passport = require("passport");

var jwt = require("jsonwebtoken");

var jwtConfig = require("../config/jwtConfig");

var models = require("../schema/models");

var User = models.User;

var util = require("../util/util");

var router = express.Router(); // This all happens on /api/auth

router.post("/login", function (req, res, next) {
  var code = req.body.code;
  var email = req.body.email;

  if (code) {
    passport.authenticate("login", function (err, user, info) {
      if (err) return next(err);

      if (!user || !user.isCodeValid()) {
        return res.status(401).send([user, "Cannot log in", info]);
      }

      user.resetAttempts();
      user.incrementVisits();
      user.save().catch(function (err) {
        console.error(err);
      });
      var payload = {
        uid: user.uid,
        expires: (0, _now.default)() + 24 * 60 * 60 * 1000
      };
      req.login(payload, {
        session: false
      }, function (err) {
        if (err) {
          res.status(500).send({
            err: err
          });
        } else {
          var token = jwt.sign((0, _stringify.default)(payload), jwtConfig.secret);
          res.status(200).send({
            token: token,
            user: user,
            message: "Welcome" + (user.visits > 1 ? " back" : "") + " :)"
          });
        }
      });
    })(req, res, next);
  } else if (email) {
    User.byEmail(email, function (err, user) {
      if (err) {
        console.error(err);
        res.status(500).send({
          message: "Had a problem looking up email"
        });
      } else if (user) {
        if (user.canGetCode()) {
          user.newCode();
          user.save().catch(function (err) {
            return console.error(err);
          });
          res.status(200).send({
            message: "Emailed temporary code"
          });
        } else {
          res.status(429).send();
        }
      } else {
        User.create({
          email: email
        }, function (err1, newUser) {
          if (newUser) {
            newUser.newCode();
            newUser.save();
            res.status(200).send({
              message: "Emailed temporary code"
            });
          } else {
            if (err1) console.error(err1);
            res.status(500).send({
              message: "Had a problem creating user"
            });
          }
        });
      }
    });
  } else {
    res.status(400).send({
      message: "An email is required. Are you using x-www-form-urlencoded?"
    });
  }
});
router.get("/logout", function (req, res) {
  req.logout();
  return res.status(200).send({
    message: "Logged out"
  });
});
router.get("/user", passport.authenticate("JWT", {
  session: false
}), function (req, res) {
  res.send({
    user: req.user
  });
});
module.exports = router;