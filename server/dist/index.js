"use strict";

var _util = require("./util/util");

/* eslint-disable no-console */
var cors = require("cors");

var express = require("express");

var passport = require("passport");

var bodyParser = require("body-parser");

var serveStatic = require("serve-static");

// Configuration
//const app = require("./config/secure");
var app = express();

require("./config/passport");

require("./config/mongo");

require("./config/jwtConfig"); // Middleware


app.use(bodyParser.json({
  limit: "50mb",
  extended: true
}));
app.use(bodyParser.urlencoded({
  limit: "50mb",
  extended: true
}));
app.use(cors());
app.use(passport.initialize()); // Start using passport

app.use(passport.session()); // Session management
// Routes
// Catch all routes and redirect to the index file

console.log(_util.clientDist);
app.use("/", serveStatic(_util.clientDist));

var staticRoute = require("./api/static");

app.use("/", staticRoute);

var auth = require("./api/auth");

app.use("/api/auth", auth);

var project = require("./api/project");

app.use("/api/project", project);
var port = process.env.PORT || 5000;
app.listen(port, function () {
  return console.log("Server started on port ".concat(port));
});