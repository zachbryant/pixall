"use strict";

var nodemailer = require("nodemailer");

var createTransport = nodemailer.createTransport;

var emailConfig = require("../config/mail");

var transporter = createTransport(emailConfig.transport);

function sendLoginCode(email, code) {
  transporter.sendMail(emailConfig.loginEmailOptions(email, code), function (err, info) {
    if (err || !info) {
      console.error("Could not send mail:");
      if (err) console.error(err);
    }
  });
}

module.exports = {
  emailConfig: emailConfig,
  sendLoginCode: sendLoginCode
};