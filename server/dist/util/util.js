"use strict";

var _interopRequireDefault = require("/home/zach/mega/repos/web/pixall/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

var _now = _interopRequireDefault(require("/home/zach/mega/repos/web/pixall/node_modules/@babel/runtime-corejs2/core-js/date/now"));

var _v = _interopRequireDefault(require("uuid/v4"));

var path = require("path");

function getUUIDV4() {
  return (0, _v.default)();
}

function isValidJWT(jwt_payload) {
  return !jwt_payload.expires || jwt_payload.expires > (0, _now.default)();
}

var clientDist = path.resolve(__dirname, "..", "../../client/dist");
module.exports = {
  isValidJWT: isValidJWT,
  getUUIDV4: getUUIDV4,
  clientDist: clientDist
};