"use strict";

var mongoose = require("mongoose");

var mongoRemote = "mongodb+srv://dbuser:xMCrCU564jW7OpwH@cluster0-8llb7.mongodb.net/test?retryWrites=true"; // Default connection to the db

mongoose.connect(mongoRemote, {
  useNewUrlParser: true
});
var db = mongoose.connection; // On Error event

db.on("error", console.error.bind(console, "MongoDB connection error: ")); // On connected event

db.on("connected", function () {
  console.log("MongoDB Connected Successfully");
}); // On Disconnected event

db.on("disconnected", function () {
  console.log("MongoDB Disconnected");
});
module.exports = {
  mongoRemote: mongoRemote,
  db: db
};