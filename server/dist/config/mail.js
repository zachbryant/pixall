"use strict";

var fromHeader = '"Pixall" <no-reply@pixall.top>';
module.exports = {
  transport: {
    host: "mail.privateemail.com",
    port: 465,
    secure: true,
    auth: {
      user: "no-reply@pixall.top",
      pass: "pixalllab6"
    }
  },
  options: {
    from: fromHeader
  },
  loginEmailOptions: function loginEmailOptions(email, code) {
    return {
      from: fromHeader,
      subject: "🎨 Pixall temporary login code",
      to: email,
      html: "<div\n        style=\"margin: 0 auto;width: 100%; max-width: 600px;font-weight:500; color: #333333;font-family:sans-serif;\"\n      >\n        <h4>Howdy,</h4>\n        <p>Your temporary login code is</p>\n        <pre\n          style=\"font-family:monospace;background-color:#F4F4F4;border-radius:3px;margin-bottom:24px;padding:16px 24px;border:1px solid #EEEEEE;\"\n\t\t\t\t><span id=\"pixcode\">".concat(code, "</span><img id=\"clip\" src=\"https://i.imgur.com/F9kGJru.png\" style=\"float:right;\"></img></pre>\n\t\t\t\t<p>If you didn't request this code, you can safely ignore this email.</p>\n\t\t\t\t&nbsp;\n        <p>Best,</p>\n\t\t\t\t<p style=\"font-weight: 400;font-style: italic;\">The Pixall Team</p>\n\t\t\t\t<a style=\"color: #9A6BB0;\" href=\"mailto:admin@pixall.top\">Contact Us</a>\n      </div>\n  ")
    };
  }
};