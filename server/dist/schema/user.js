"use strict";

var _interopRequireDefault = require("/home/zach/mega/repos/web/pixall/node_modules/@babel/runtime-corejs2/helpers/interopRequireDefault");

require("core-js/modules/es6.regexp.constructor");

require("core-js/modules/es6.array.find");

require("core-js/modules/es6.number.constructor");

var _mongoose = require("mongoose");

var _v = _interopRequireDefault(require("uuid/v4"));

var code_gen = require("../util/code_gen");

var mailer = require("../util/mail");

var timerMap = {};
var userSchema = new _mongoose.Schema({
  uid: {
    type: String,
    default: (0, _v.default)()
  },
  email: {
    type: String,
    index: true,
    unique: true,
    required: true
  },
  code: {
    type: String,
    default: ""
  },
  attempts: {
    type: Number,
    default: 0
  },
  lastAttempt: {
    type: Date,
    default: null
  },
  visits: {
    type: Number,
    default: 0
  }
}); // For doing general work on "Users"

userSchema.statics = {
  byUid: function byUid(uid, callback) {
    return this.findOne({
      uid: uid
    }, callback);
  },
  byUidArray: function byUidArray(uids, callback) {
    return this.find({
      uid: {
        $in: uids
      }
    }, callback);
  },
  byEmail: function byEmail(email, callback) {
    return this.findOne({
      email: new RegExp(email, "i")
    }, callback);
  }
}; // For doing instance work on a User

userSchema.methods = {
  clearCode: function clearCode() {
    this.code = "";
    this.markModified("code");
  },
  setCode: function setCode(code) {
    this.code = code;
    this.markModified("code");
    this.incrementAttempts();
    var prevTimer = timerMap[this.uid];

    if (prevTimer) {
      clearTimeout(prevTimer);
    }

    var self = this;
    timerMap[this.uid] = setTimeout(function () {
      self.clearCode();
    }, 5 * 60 * 1000);
  },
  newCode: function newCode(callback) {
    if (!this.canGetCode()) {
      callback({
        message: "Cool your jets! Wait 3 minutes."
      });
    } else {
      var code = code_gen.randPassword();
      this.setCode(code, callback);
      mailer.sendLoginCode(this.email, code);
    }
  },
  incrementVisits: function incrementVisits() {
    this.visits++;
    this.markModified("visits");
  },
  incrementAttempts: function incrementAttempts() {
    this.attempts++;
    if (this.canGetCode()) this.lastAttempt = new Date();
    this.markModified("attempts");
    this.markModified("lastAttempt");
  },
  resetAttempts: function resetAttempts() {
    this.attempts = 0;
    this.markModified("attempts");
  },
  isOverLimit: function isOverLimit() {
    return this.attempts >= 5;
  },
  minsSinceLastAttempt: function minsSinceLastAttempt() {
    var lastDate = this.lastAttempt || new Date();
    var diffMs = new Date() - lastDate;
    return Math.round(diffMs % 86400000 % 3600000 / 60000);
  },
  canGetCode: function canGetCode() {
    return !(this.isOverLimit() && this.minsSinceLastAttempt() < 3);
  },
  isCodeValid: function isCodeValid() {
    return this.minsSinceLastAttempt() < 5;
  }
};
var User = (0, _mongoose.model)("User", userSchema, "Users");
module.exports = User;