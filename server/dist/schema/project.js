"use strict";

require("core-js/modules/es6.array.find");

var _mongoose = require("mongoose");

var projectSchema = new _mongoose.Schema({
  uid: String,
  pid: String,
  data: _mongoose.Schema.Types.Mixed
});
projectSchema.index({
  uid: 1,
  pid: 1
});
projectSchema.statics = {
  byUser: function byUser(user, callback) {
    return this.find({
      uid: user instanceof Object ? user.uid : user
    }, callback);
  },
  byProject: function byProject(project, callback) {
    return this.find({
      pid: project instanceof Object ? project.pid : project
    }, callback);
  },
  hasAccess: function hasAccess(user, project, callback) {
    return this.findOne({
      uid: user instanceof Object ? user.uid : user,
      pid: project instanceof Object ? project.pid : project
    }, callback);
  }
};
var Project = (0, _mongoose.model)("Project", projectSchema, "Projects");
module.exports = Project;